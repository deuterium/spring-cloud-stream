package cloud.deuterium.stream.order.controller;

import cloud.deuterium.stream.order.model.Order;
import cloud.deuterium.stream.order.service.OrderService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * Created by MilanNuke 12-Oct-19
 */

@RestController
@RequestMapping("/orders")
public class OrderController {

    private Logger logger = LoggerFactory.getLogger(OrderController.class);
    private ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private OrderService orderService;

    @PostMapping
    public ResponseEntity<Order> process(@RequestBody Order order) throws JsonProcessingException {
        logger.info("Order saved: {}", mapper.writeValueAsString(order));
        Order processedOrder = orderService.processOrder(order);
        logger.info("Order sent: {}", mapper.writeValueAsString(processedOrder));
        return ResponseEntity.ok(processedOrder);
    }

}
