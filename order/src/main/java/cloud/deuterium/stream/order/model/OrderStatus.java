package cloud.deuterium.stream.order.model;

/**
 * Created by MilanNuke 12-Oct-19
 */
public enum OrderStatus {
    NEW, PROCESSING, ACCEPTED, DONE, REJECTED;
}
