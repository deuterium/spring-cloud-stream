package cloud.deuterium.stream.order.service;

import cloud.deuterium.stream.order.model.Order;

/**
 * Created by MilanNuke 12-Oct-19
 */
public interface OrderService {
    Order processOrder(Order order);
}
