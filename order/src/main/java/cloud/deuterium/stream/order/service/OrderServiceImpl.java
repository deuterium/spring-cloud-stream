package cloud.deuterium.stream.order.service;

import cloud.deuterium.stream.order.model.Order;
import cloud.deuterium.stream.order.model.OrderStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Service;

/**
 * Created by MilanNuke 12-Oct-19
 */

@EnableBinding(OrderSource.class)
@Service
public class OrderServiceImpl implements OrderService {

    private Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Autowired
    private OrderSource orderSource;

    @Override
    public Order processOrder(Order order) {
        logger.info("Sending order {}", order.getId());
        order.setStatus(OrderStatus.NEW);
        boolean send = orderSource.orderProcessing().send(MessageBuilder.withPayload(order).build());
        if(send){
            logger.info("Order {} successfully sent", order.getId());
            return order;
        }else {
            logger.info("Order {} sent failed", order.getId());
            order.setStatus(OrderStatus.REJECTED);
            return order;
        }
    }
}
