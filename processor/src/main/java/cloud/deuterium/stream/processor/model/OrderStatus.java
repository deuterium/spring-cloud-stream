package cloud.deuterium.stream.processor.model;

/**
 * Created by MilanNuke 12-Oct-19
 */
public enum OrderStatus {
    NEW, PROCESSING, ACCEPTED, DONE, REJECTED;
}
