package cloud.deuterium.stream.processor.service;

import cloud.deuterium.stream.processor.model.Order;
import cloud.deuterium.stream.processor.model.OrderStatus;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Processor;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.integration.annotation.Transformer;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

/**
 * Created by MilanNuke 13-Oct-19
 */

@EnableBinding(Processor.class)
@Component
public class ProcessorService {

    private ObjectMapper mapper = new ObjectMapper();
    private Logger logger = LoggerFactory.getLogger(this.getClass());

//    @SendTo(Processor.OUTPUT)
//    @StreamListener(Processor.INPUT)
    @Transformer(inputChannel = Processor.INPUT, outputChannel = Processor.OUTPUT)
    public Order processOrders(Order order) throws JsonProcessingException {
        logger.info("Order received: {}", mapper.writeValueAsString(order));
        order.setStatus(OrderStatus.PROCESSING);
        order.setAccountId(123L);
        logger.info("Order processed: {}", mapper.writeValueAsString(order));
        return order;
    }

}
