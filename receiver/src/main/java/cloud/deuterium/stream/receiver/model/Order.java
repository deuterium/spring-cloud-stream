package cloud.deuterium.stream.receiver.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by MilanNuke 12-Oct-19
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Order {

    private Long id;
    private OrderStatus status;
    private int price;
    private Long customerId;
    private Long accountId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }
}
