package cloud.deuterium.stream.receiver.service;

import cloud.deuterium.stream.receiver.model.Order;
import cloud.deuterium.stream.receiver.model.OrderStatus;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.stereotype.Component;

/**
 * Created by MilanNuke 13-Oct-19
 */

@EnableBinding(Sink.class)
@Component
public class OrderListener {

    private ObjectMapper mapper = new ObjectMapper();
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @StreamListener(Sink.INPUT)
    public void processOrders(Order order) throws JsonProcessingException {
        order.setStatus(OrderStatus.ACCEPTED);
        logger.info("Order received: {}", mapper.writeValueAsString(order));
    }
}
